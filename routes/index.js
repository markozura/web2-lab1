var router = require('express').Router();
const { requiresAuth } = require('express-openid-connect');
const pool = require('../db')

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Auth0 Webapp sample Nodejs',
    isAuthenticated: req.oidc.isAuthenticated()
  });
});

router.get('/db', function (req, res) {
    let data = [];
    pool.query(`select club from teams`, (error, results1) => {
        if(error) throw error;
        else {
            pool.query(`select id_match, team1.club as home_club, home, away, team2.club as away_club
                        from teams as team1
                                 join matches on team1.id_team = matches.id_home
                                 natural join results
                                 join teams as team2 on matches.id_away = team2.id_team`, (error, results2) => {
                if (error) throw error;
                else {
                    for(let i = 0; i < results1.rows.length; i++) {
                        let goals_for = 0;
                        let goals_against = 0;
                        let points = 0;
                        let games_played = 0;
                        for(let j = 0; j < results2.rows.length; j++) {
                            if (results2.rows[j].home_club === results1.rows[i].club) {
                                games_played += 1;
                                goals_for += results2.rows[j].home;
                                goals_against += results2.rows[j].away;
                                if(results2.rows[j].home > results2.rows[j].away) {
                                    points += 3;
                                } else if (results2.rows[j].home = results2.rows[j].away) {
                                    points += 1;
                                }
                            } else if (results2.rows[j].away_club === results1.rows[i].club){
                                games_played += 1;
                                goals_for += results2.rows[j].away;
                                goals_against += results2.rows[j].home;
                                if(results2.rows[j].away > results2.rows[j].home) {
                                    points += 3;
                                } else if (results2.rows[j].home === results2.rows[j].away) {
                                    points += 1;
                                }
                            }
                        }
                        let goal_diff = goals_for - goals_against;
                        data[i] = {club: results1.rows[i].club, points: points, goal_diff: goal_diff, games_played: games_played}
                    }
                    data.sort(compareNumbers1);
                }
                res.render('leagueTable', {
                    data1: data
                });
            })
        }
    });
});

function compareNumbers1(a, b) {
    if (a.points === b.points) {
        return b.goal_diff - a.goal_diff
    }
    return b.points - a.points;
}

router.get('/results', function (req, res) {
    pool.query(`select id_match, team1.club as home_club, home, away, team2.club as away_club
                        from teams as team1
                                 join matches on team1.id_team = matches.id_home
                                 natural join results
                                 join teams as team2 on matches.id_away = team2.id_team`, (error, result) => {
        if(error) throw error;
        res.render('results', {
            data: result.rows.sort(compareNumbers2)
        });
    });
});

function compareNumbers2(a, b) {
    return a.gameweekid - b.gameweekid;
}

router.get('/schedule', (req, res) => {
    pool.query(`select id_match, team1.club as home_club, team2.club as away_club, date_time, gameweekid
                        from teams as team1
                                 join matches on team1.id_team = matches.id_home
                                 join teams as team2 on matches.id_away = team2.id_team
where id_match not in (select id_match from results)`, (error, result) => {
        if (error) throw error;
        for(let i = 0; i < result.rows.length; i++) {
            result.rows[i].date_time = result.rows[i].date_time.toLocaleDateString() + " " + result.rows[i].date_time.toLocaleTimeString();
        }
        res.render('schedule', {
            data: result.rows
        });
    });
});

router.post('/schedule', (req, res) => {
    var home_goals = req.body.home_goals;
    var away_goals = req.body.away_goals;
    var id_match = req.body.matchid;
    console.log(home_goals + " : " + away_goals + " matchid= " +id_match);
    pool.query(`insert into results values ($1, $2, $3)`, [id_match, home_goals, away_goals], (error, result) => {
        if(error) throw error;
    });

    pool.query(`select id_match, team1.club as home_club, team2.club as away_club, date_time, gameweekid
                        from teams as team1
                                 join matches on team1.id_team = matches.id_home
                                 join teams as team2 on matches.id_away = team2.id_team
                where id_match not in (select id_match from results)`, (error, result) => {
        if (error) throw error;
        for(let i = 0; i < result.rows.length; i++) {
            result.rows[i].date_time = result.rows[i].date_time.toLocaleDateString() + " " + result.rows[i].date_time.toLocaleTimeString();
        }
        res.render('schedule', {
            data: result.rows
        });
    });
})

router.get('/comm',requiresAuth(), (req, res) => {
    pool.query(`select id_match, team1.club as home_club, home, away, team2.club as away_club, gameweekid
                from teams as team1
                         join matches on team1.id_team = matches.id_home
                         natural join results
                         join teams as team2 on matches.id_away = team2.id_team`, (error, result) => {
        if(error) throw error;
        pool.query(`select * from review`, (error, result2) => {
            if(error) throw error;
            if(result2.rows[0] !== undefined) {
                result2.rows.sort(compareNumbers2);
                for (let i = 1; i <= result2.rows[result2.rows.length - 1].gameweekid; i++) {
                    let comments = [];
                    for (let j = 0; j < result2.rows.length; j++) {
                        if (i === result2.rows[j].gameweekid) {
                            if (result2.rows[j].email === req.oidc.user.email) {
                                comments.push({
                                    comm: result2.rows[j].comment,
                                    email: result2.rows[j].email,
                                    date_time: result2.rows[j].date_time.toLocaleDateString() + " " + result2.rows[j].date_time.toLocaleTimeString(),
                                    owner: true,
                                    id_review: result2.rows[j].id_review
                                });
                            } else {
                                comments.push({
                                    comm: result2.rows[j].comment,
                                    email: result2.rows[j].email,
                                    date_time: result2.rows[j].date_time.toLocaleDateString() + " " + result2.rows[j].date_time.toLocaleTimeString(),
                                    owner: false,
                                    id_review: result2.rows[j].id_review
                                });
                            }
                        }
                    }
                    for (let k = 0; k < result.rows.length; k++) {
                        if (result.rows[k].gameweekid === i) {
                            result.rows[k]["comments"] = comments;
                            let s = 0;
                            comments.forEach((review) => {
                                if (review.email === req.oidc.user.email) s = 1;
                            })
                            if (s === 1) {
                                result.rows[k]["owner"] = true;
                            }
                        }
                    }
                }
            }
            res.render('resComm', {
                data: result.rows.sort(compareNumbers2),
                user: req.oidc.user
            });
        });
    });
});

router.post('/comm', (req, res) => {
    var comm;
    var email;
    var gw;
    const date = new Date();
    var method = req.body.method;
    if(method === "put") {
        comm = req.body.comm;
        var reviewid = req.body.reviewid;
        pool.query(`update review
                        set comment = $1
                        where id_review = $2`, [comm, reviewid], (error, result) => {
            if(error) throw error;
        });

    } else if (method === "post") {
        comm = req.body.comm;
        email = req.body.email;
        gw = req.body.gw;
        pool.query(`insert into review (gameweekid, email, comment, date_time)
                    values ($1, $2, $3, $4)`, [gw, email, comm, date], (error, result) => {
            if (error) throw error;
        });
    } else if (method === "delete") {
        var reviewid = req.body.reviewid;
        pool.query(`delete from review where id_review = $1`, [reviewid], (error, result) => {
            if(error) throw error;
        });
    }

    pool.query(`select id_match, team1.club as home_club, home, away, team2.club as away_club, gameweekid
                from teams as team1
                         join matches on team1.id_team = matches.id_home
                         natural join results
                         join teams as team2 on matches.id_away = team2.id_team`, (error, result) => {
        if(error) throw error;
        pool.query(`select * from review`, (error, result2) => {
            if(error) throw error;
            if(result2.rows[0] !== undefined) {
                result2.rows.sort(compareNumbers2);
                for (let i = 1; i <= result2.rows[result2.rows.length - 1].gameweekid; i++) {
                    let comments = [];
                    for (let j = 0; j < result2.rows.length; j++) {
                        if (i === result2.rows[j].gameweekid) {
                            if (result2.rows[j].email === req.oidc.user.email) {
                                comments.push({
                                    comm: result2.rows[j].comment,
                                    email: result2.rows[j].email,
                                    date_time: result2.rows[j].date_time.toLocaleDateString() + " " + result2.rows[j].date_time.toLocaleTimeString(),
                                    owner: true,
                                    id_review: result2.rows[j].id_review
                                });
                            } else {
                                comments.push({
                                    comm: result2.rows[j].comment,
                                    email: result2.rows[j].email,
                                    date_time: result2.rows[j].date_time.toLocaleDateString() + " " + result2.rows[j].date_time.toLocaleTimeString(),
                                    owner: false,
                                    id_review: result2.rows[j].id_review
                                });
                            }
                        }
                    }
                    for (let k = 0; k < result.rows.length; k++) {
                        if (result.rows[k].gameweekid === i) {
                            result.rows[k]["comments"] = comments;
                            let s = 0;
                            comments.forEach((review) => {
                                if (review.email === req.oidc.user.email) s = 1;
                            })
                            if (s === 1) {
                                result.rows[k]["owner"] = true;
                            }
                        }
                    }
                }
            }
            res.render('resComm', {
                data: result.rows.sort(compareNumbers2),
                user: req.oidc.user
            });
        });
    });


});

router.put('/comm/:id', (req, res) => {
    let id = +req.params.id;
    let body = req.body;
})


router.get('/profile', requiresAuth(), function (req, res, next) {
  res.render('profile', {
    userProfile: JSON.stringify(req.oidc.user, null, 2),
    title: 'Profile page'
  });
});

module.exports = router;
